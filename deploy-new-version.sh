#!/bin/bash

##########
# Deploy new version
##########

set -e # Exit on error
#set -x # See what is happening when executing the script.

# Make sure user specifies the new version
if [ $# -ne 1 ]; then
	echo "Please specify the new version."
	echo "Example: ./deploy-new-version.sh 1.5.0"
    exit 1
fi

git pull
cp LICENSE 1_9/
cp README.md 1_9/README
cp LICENSE 2_1_X/
cp README.md 2_1_X/README
sed -i "s/const VERSION = '.*';/const VERSION = '$1';/" 1_9/lib/B1/B1.php
sed -i "s/const PLATFORM = '.*';/const PLATFORM = 'magento';/" 1_9/lib/B1/B1.php
git commit -am "Bump plugin version"
git push
git tag -a $1 -m "New version"
git push --tags