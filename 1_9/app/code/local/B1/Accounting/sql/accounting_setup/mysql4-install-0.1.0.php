<?php

$installer = $this;
$setup = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();
$connection = $installer->getConnection();
$tableNameProduct = $installer->getTable('catalog_product_entity');
$columnNameProduct = 'b1_reference_id';
if ($connection->tableColumnExists($tableNameProduct, $columnNameProduct) === false) {
    $connection->addColumn($tableNameProduct, $columnNameProduct, array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'default' => null,
        'comment' => 'B1',
    ));
}

$tableNameSales = $installer->getTable('sales_flat_order');
$columnNameSales = 'b1_reference_id';
if ($connection->tableColumnExists($tableNameSales, $columnNameSales) === false) {
    $connection->addColumn($tableNameSales, $columnNameSales, array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'B1',
    ));
}

$installer->endSetup();
