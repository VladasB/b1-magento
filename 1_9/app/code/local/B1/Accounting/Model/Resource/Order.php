<?php

class B1_Accounting_Model_Resource_Order extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('accounting_resource/order', 'entity_id');

    }
    public function abc(){
      return 123;
    }

    public function findAllToSync()
    {
        $statusName = Mage::getStoreConfig('accounting/config/order_status_name_check');
        $orderSyncFrom = Mage::getStoreConfig('accounting/config/order_sync_from');
        return var_dump($this->getMainTable('accounting_resource/order'));
        $sql = "SELECT * FROM {$this->getTable('sales/order')} WHERE `b1_reference_id` IS NULL AND `created_at`>=:f AND (status=:os OR entity_id IN (SELECT parent_id FROM {$this->getTable('sales/order_status_history')} WHERE status=:os)) LIMIT 100";
$a=  $this->_getWriteAdapter()->query($sql, array(
            'os' => $statusName,
            'f' => $orderSyncFrom,
        ));
//print_r($a);exit;

        return $a;
    }

    public function updateB1ReferenceId($orderId, $referenceId)
    {
        if ($referenceId == null) {
            $sql = "UPDATE {$this->getTable('sales/order')} SET `b1_reference_id` = NULL WHERE `entity_id` = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'o' => $orderId,
            ));
        } else {
            $sql = "UPDATE {$this->getTable('sales/order')} SET `b1_reference_id` = :r WHERE `entity_id` = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'r' => $referenceId,
                'o' => $orderId,
            ));
        }
        return $this;
    }

    public function getSyncData($writeOff = true)
    {
        echo 123;
        $shopId = Mage::getStoreConfig('accounting/config/shop_id');
        $mask = Mage::getStoreConfig('accounting/config/invoice_product_name_mask');
        $items = $this->getAllVisibleItems();
        $billingAddress = $this->getBillingAddress();
        $shippingAddress = $this->getShippingAddress();
        $firstItem = $items[0];
        $data = array(
            'prefix' => $shopId,
            'orderid' => $this->getId(),
            'orderdate' => date('Y-m-d', strtotime($this->getCreatedAt())),
            'orderno' => $this->getId(),
            'currency' => $this->getOrderCurrencyCode(),
            'shippingAmount' => $this->convertFloatToInt($this->getShippingAmount() + $this->getShippingTaxAmount()),
            'discount' => $this->convertFloatToInt($this->getDiscountAmount()),
            'total' => $this->convertFloatToInt($this->getGrandTotal()),
            'orderemail' => $this->getCustomerEmail(),
            'vatrate' => intval(round($firstItem->getTaxPercent())),
            'writeoff' => $writeOff ? 1 : 0,
            'billing' => array(
                'name' => $billingAddress->getName(),
                'iscompany' => $billingAddress->getCompany() != '' ? 1 : 0,
                'vatcode' => $billingAddress->getVatId(),
                'address' => $billingAddress->getStreet1(),
                'city' => $billingAddress->getCity(),
                'postcode' => $billingAddress->getPostcode(),
                'country' => $billingAddress->getCountryId(),
            ),
            'delivery' => array(
                'name' => $shippingAddress->getName(),
                'iscompany' => $shippingAddress->getCompany() != '' ? 1 : 0,
                'address' => $shippingAddress->getStreet1(),
                'city' => $shippingAddress->getCity(),
                'postcode' => $shippingAddress->getPostcode(),
                'country' => $shippingAddress->getCountryId(),
            ),
        );
        foreach ($items as $i => $item) {
            $name = str_replace(array('{name}', '{model}', '{sku}', '{upc}', '{ean}', '{jan}', '{isbn}', '{mpn}'), array($item->getName(), 'MODEL_NOT_SUPPORTED', $item->getSku(), 'UPC_NOT_SUPPORTED', 'EAN_NOT_SUPPORTED', 'JAN_NOT_SUPPORTED', 'ISBN_NOT_SUPPORTED', 'MPN_NOT_SUPPORTED'), $mask);
            $product = $item->getProduct();
            $data['items'][$i] = array(
                'id' => $product['b1_reference_id'],
                'name' => $name,
                'quantity' => $this->convertFloatToInt($item->getQtyOrdered()),
                'price' => $this->convertFloatToInt($item->getPriceInclTax()),
                'sum' => $this->convertFloatToInt($item->getRowTotalInclTax()),
                'vatrate' => intval(round($item->getTaxPercent())),
            );
        }
        return $data;
    }

}
