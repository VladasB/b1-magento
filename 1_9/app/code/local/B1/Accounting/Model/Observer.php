<?php

require_once(Mage::getBaseDir('lib') . '/B1/B1.php');

class B1_Accounting_Model_Observer
{

    const TTL = 3600;

    public function __construct()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);
    }

    public function syncAllProducts()
    {
        try {
            $this->syncB1ProductsWithShop(true);
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncUpdatedProducts()
    {
        try {
            $this->syncB1ProductsWithShop(false);
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    public function syncOrders()
    {

        try {
            $this->syncShopOrdersWithB1();
        } catch (Exception $ex) {
            Mage::logException($ex);
        }
        return $this;
    }

    /**
     * @param bool $syncAll
     * @return $this
     * @throws B1Exception
     */
    private function syncB1ProductsWithShop($syncAll = true)
    {
        $iteration = 0;
        $lastId = null;
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');
        $maxRequestsToApi = Mage::getStoreConfig('accounting/config/max_requests_to_api_per_session');
        $listSize = Mage::getStoreConfig('accounting/config/list_size');
        $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');
        $modAfterDate = $syncAll ? null : Mage::getStoreConfig('accounting/config/latest_product_sync_date');
        $b1 = new B1(['apiKey' => $apiKey, 'privateKey' => $privateKey]);
        $model = Mage::getModel('accounting/product');
        if ($syncAll) {
            $model->resetAllB1ReferenceId();
        }
        do {

            $iteration++;
            $response = $b1->request('shop/product/list', ["lid" => $lastId, "lmod" => $modAfterDate]);
            $data = $response->getContent();
            foreach ($data['data'] as $product) {
                $lastId = $product['id'];
                $model->updateCode($product['code'], $product['id']);
                if ($enableQuantitySync) {
                    $model->updateQuantity($product);
                }
            }
        } while (!empty($data) && count($data) == $listSize && $iteration < $maxRequestsToApi);
        if ($syncAll) {
            Mage::getConfig()->saveConfig('accounting/config/initial_product_sync_done', 1)->cleanCache();
        }else{
            Mage::getConfig()->saveConfig('accounting/config/latest_product_sync_date',date("Y-m-d"));
        }
        return $this;
    }

    /**
     * @return $this|bool
     * @throws B1Exception|Exception
     */
    private function syncShopOrdersWithB1()
    {
        $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
        if (!$initialProductSyncDone) {
            throw new Exception("Initial product sync is not done yet.");
        }
        $initialOrderSyncDone = Mage::getStoreConfig('accounting/config/initial_order_sync_done');
        $writeOff = $initialOrderSyncDone ? Mage::getStoreConfig('accounting/config/order_writeoff') : false;
        $enableQuantitySync = Mage::getStoreConfig('accounting/config/enable_quantity_sync');
        $apiKey = Mage::getStoreConfig('accounting/config/api_key');
        $privateKey = Mage::getStoreConfig('accounting/config/private_key');
        $b1 = new B1(['apiKey' => $apiKey, 'privateKey' => $privateKey]);
        $model = Mage::getModel('accounting/product');
        $modelOrder = Mage::getModel('accounting/order');
        $orders = $modelOrder->findAllToSync();
var_dump($orders);
        foreach ($orders as $order) {

            $data = $order->abc();
var_dump($data);
            try {
                $result = $b1->request('shop/order/add', $data);
                $data = $result->getContent();
                $order->updateB1ReferenceId($data['data']['orderId']);
                if ($enableQuantitySync && isset($data['data']['update'])) {
                    foreach ($data['data']['update'] as $productQuantity) {
                        $model->updateQuantity($productQuantity);
                    }
                }
            } catch (B1DuplicateException $e) {
                $content = $e->getResponse()->getContent();
                $order->updateB1ReferenceId($content['data']['orderId']);
            }
        }
        if (!$initialOrderSyncDone) {
            Mage::getConfig()->saveConfig('accounting/config/initial_order_sync_done', 1)->cleanCache();
        }
        return $this;
    }

}
