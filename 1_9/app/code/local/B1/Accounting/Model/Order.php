<?php

class B1_Accounting_Model_Order extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {

        $this->_init('accounting/order');
    }

    public function findAllToSync()
    {
        return $this->getResource()->findAllToSync();
    }

    public function abc()
    {
        return 12;
        return $this->getResource()->getSyncData($writeOff);
    }

    public function updateB1ReferenceId($referenceId)
    {
        $this->getResource()->updateB1ReferenceId($this->getId(), $referenceId);
        return $this;
    }

    private function convertFloatToInt($value)
    {
        return intval(round($value * 100));
    }

}
