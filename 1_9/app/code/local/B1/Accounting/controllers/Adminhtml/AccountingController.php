<?php
require_once(Mage::getBaseDir('lib') . '/B1/B1.php');

class B1_Accounting_Adminhtml_AccountingController extends Mage_Adminhtml_Controller_Action
{

    public function failedOrdersAction()
    {
        $this->addInfoMessages();
        $this->loadLayout()
            ->_setActiveMenu('accounting/failedOrders');
        $this->_addContent(
            $this->getLayout()->createBlock('accounting/adminhtml_failedOrders')
        );
        $this->renderLayout();
    }

    public function failedOrdersGridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('accounting/adminhtml_failedOrders_grid')->toHtml()
        );
    }

    private function addInfoMessages()
    {
        Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('adminhtml')->__('- "Orders failed to sync" lists all orders that failed to sync more than the maximum allowed times. Orders in this list will not be synced automatically with B1. You should inspect each order and try to figure out why sync failed, then reset the order, by pressing the reset button near the fixed order. To figure out what went wrong, by default, B1 keeps track of errors and various debug information in database, b1_log table.'));
        $initialProductSyncDone = Mage::getStoreConfig('accounting/config/initial_product_sync_done');
        if (!$initialProductSyncDone) {
            Mage::getSingleton('adminhtml/session')->addWarning(Mage::helper('adminhtml')->__('Initial sync is in progress. Please wait while products are being fetched from B1.'));
        }
    }

}