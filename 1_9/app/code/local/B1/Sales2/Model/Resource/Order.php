<?php

require_once('app/code/local/B1/Sales/Model/Order/b1ClassResources.php');


class B1_Sales_Model_Resource_Order extends Mage_Sales_Model_Resource_Order
{

    public function findAllToSync()
    {
        $statusName = Mage::getStoreConfig('accounting/config/order_status_name_check');
        $orderSyncFrom = Mage::getStoreConfig('accounting/config/order_sync_from');
        $sql = "SELECT * FROM {$this->getMainTable()} WHERE `b1_reference_id` IS NULL AND `created_at`>=:f AND (status=:os OR entity_id IN (SELECT parent_id FROM {$this->getTable('sales/order_status_history')} WHERE status=:os)) LIMIT 100";
        $this->_getWriteAdapter()->query($sql, array(
            'os' => $statusName,
            'f' => $orderSyncFrom,
        ));
        $orders = Mage::getModel('sales/order')
            ->getCollection();
        return $orders;
    }

    public function updateB1ReferenceId($referenceId)
    {

        if ($referenceId == null) {
            $sql = "UPDATE {$this->getMainTable()} SET `b1_reference_id` = NULL WHERE `entity_id` = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'o' => $this->getId(),
            ));
        } else {
            $sql = "UPDATE {$this->getMainTable()} SET `b1_reference_id` = :r WHERE `entity_id` = :o";
            $this->_getWriteAdapter()->query($sql, array(
                'r' => $referenceId,
                'o' => $this->getId(),
            ));
        }
        return $this;
    }

}