<?php


/**
 * @method B1_Sales_Model_Resource_Order getResource()
 * @method Model_Order
 */
class B1_Sales_Model_Order extends Model_Order
{

    public function findAllToSync()
    {
        return $this->getResource()->findAllToSync();
    }

    public function getSyncData($writeOff = true)
    {
        $shopId = Mage::getStoreConfig('accounting/config/shop_id');
        $mask = Mage::getStoreConfig('accounting/config/invoice_product_name_mask');
        $a= new Model_Order;
        $items = $a->getAllVisibleItems();
       // print_r($this);
        $billingAddress = $a->getBillingAddress();
       // var_dump($billingAddress);
        $shippingAddress = Model_Order::getShippingAddress();
        $firstItem = $items[0];
        $data = array(
            'prefix' => $shopId,
            'orderid' => $this->getId(),
            'orderdate' => date('Y-m-d', strtotime($this->getCreatedAt())),
            'orderno' => $this->getId(),
            'currency' => $this->getOrderCurrencyCode(),
            'shippingAmount' => $this->convertFloatToInt($this->getShippingAmount() + $this->getShippingTaxAmount()),
            'discount' => $this->convertFloatToInt($this->getDiscountAmount()),
            'total' => $this->convertFloatToInt($this->getGrandTotal()),
            'orderemail' => $this->getCustomerEmail(),
            'vatrate' => intval(round($firstItem->getTaxPercent())),
            'writeoff' => $writeOff ? 1 : 0,
            'billing' => array(
                'name' => $billingAddress->getName(),
                'iscompany' => $billingAddress->getCompany() != '' ? 1 : 0,
                'vatcode' => $billingAddress->getVatId(),
                'address' => $billingAddress->getStreet1(),
                'city' => $billingAddress->getCity(),
                'postcode' => $billingAddress->getPostcode(),
                'country' => $billingAddress->getCountryId(),
            ),
            'delivery' => array(
                'name' => $shippingAddress->getName(),
                'iscompany' => $shippingAddress->getCompany() != '' ? 1 : 0,
                'address' => $shippingAddress->getStreet1(),
                'city' => $shippingAddress->getCity(),
                'postcode' => $shippingAddress->getPostcode(),
                'country' => $shippingAddress->getCountryId(),
            ),
        );
        foreach ($items as $i => $item) {
            $name = str_replace(array('{name}', '{model}', '{sku}', '{upc}', '{ean}', '{jan}', '{isbn}', '{mpn}'), array($item->getName(), 'MODEL_NOT_SUPPORTED', $item->getSku(), 'UPC_NOT_SUPPORTED', 'EAN_NOT_SUPPORTED', 'JAN_NOT_SUPPORTED', 'ISBN_NOT_SUPPORTED', 'MPN_NOT_SUPPORTED'), $mask);
            $product = $item->getProduct();
            $data['items'][$i] = array(
                'id' => $product['b1_reference_id'],
                'name' => $name,
                'quantity' => $this->convertFloatToInt($item->getQtyOrdered()),
                'price' => $this->convertFloatToInt($item->getPriceInclTax()),
                'sum' => $this->convertFloatToInt($item->getRowTotalInclTax()),
                'vatrate' => intval(round($item->getTaxPercent())),
            );
        }
        var_dump($data);
        //return $data;
    }

    public function updateB1ReferenceId($referenceId)
    {

        $this->getResource()->updateB1ReferenceId($this->getId(), $referenceId);
        return $this;
    }

    private function convertFloatToInt($value)
    {
        return intval(round($value * 100));
    }

}