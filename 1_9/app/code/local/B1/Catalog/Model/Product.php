<?php

require_once('Mage/Catalog/Model/Product.php');

/**
 * @method B1_Catalog_Model_Resource_Product getResource()
 */
class B1_Catalog_Model_Product extends Mage_Catalog_Model_Product
{

    public function resetAllB1ReferenceId()
    {
        $this->getResource()->resetAllB1ReferenceId();
        return $this;
    }

    public function updateCode($code, $referenceId)
    {
        $this->getResource()->updateCode($code, $referenceId);
        return $this;
    }

    public function updateQuantity($product)
    {
        $this->getResource()->updateQuantity($product);
        return $this;
    }

}