<?php

require_once('Mage/Catalog/Model/Resource/Product.php');

class B1_Catalog_Model_Resource_Product extends Mage_Catalog_Model_Resource_Product
{

    public function resetAllB1ReferenceId()
    {
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} SET `b1_reference_id`=:r", array(
            'r' => null,
        ));
        return $this;
    }

    public function updateCode($code, $referenceId)
    {
        $referenceId = (int)$referenceId;
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('catalog/product')} SET `b1_reference_id`=:r WHERE `sku`=:id", array(
            'r' => $referenceId,
            'id' => $code,
        ));
        return $this;
    }

    public function updateQuantity($product)
    {
        $id = (int)$product['id'];
        $quantity = (int)$product['quantity'];
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_item')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        $this->_getWriteAdapter()->query("UPDATE {$this->getTable('cataloginventory/stock_status')} s LEFT OUTER JOIN {$this->getTable('catalog/product')} p ON s.`product_id` = p.`entity_id` SET s.`qty` = :q WHERE p.`b1_reference_id`=:id", array(
            'q' => $quantity,
            'id' => $id,
        ));
        return $this;
    }

}